<style>
#blog-image-picker .overlay {
  position: absolute;
  top: 0; bottom: 0;
  left: 0; right: 0;
}

.box .photos-row {
  padding: 0 1em; }
  .box .photos-row > div {
    background-color: white;
    cursor: move;
    height: 150px;
    position: relative;
    text-align: center;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
    margin: 5px;
    float: left; }
    @media only screen and (min-width: 993px) {
      .box .photos-row > div {
        width: calc(25% - 10px); } }
    .box .photos-row > div img {
      max-height: 100%;
      max-width: 100%;
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      transition: all .15s linear; }
    .box .photos-row > div:hover img {
      transform: translate(-50%, -50%) scale(0.925); }
    .box .photos-row > div span.title {
      position: absolute;
      bottom: 0;
      left: 0;
      display: inline-block;
      width: 100%;
      text-align: center;
      background-color: white; }
    .box .photos-row > div span.action {
      position: absolute;
      top: 2px;
      right: 2px;
      z-index: 48;
      background-color: rgba(255, 255, 255, 0.85); }
      .box .photos-row > div span.action.action-left {
        left: 2px;
        right: auto; }
      .box .photos-row > div span.action .fa {
        margin: 0 3px;
        cursor: pointer; }

.banner-uploader {
  padding: 0 0 1em; }
  .banner-uploader input {
    display: inline-block; }
</style>