	<div class="text-center banner-uploader">
		<input type="file" id="fileinput" accept="image/*">
	</div>
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-body">
 				<div id="blog-image-picker">
					<div class="row photos-row"></div>
					<div class="overlay hide">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
        		</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  			integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  			crossorigin="anonymous"></script>
  	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<script>
		var imagePickerRoot  = "{{ \Modules\Bannerphoto\Entities\BannerPhoto::getImageDirUrl() }}/"; // root folder of images
		var imagePickerFiles = {  // array of images
		@foreach($bannerphotos as $image) "{{ $image->position }}": {
			name: "{{ $image->image_name }}",
			id: "{{ $image->id }}",
			img: "{{ $image->thumbnail }}"
			},@endforeach
		};

		function updateImages() {
			$("#blog-image-picker .photos-row").html("");
			for (var position in imagePickerFiles) {
				var image =imagePickerFiles[position];
				var filename = image.name;
				if (filename.substr(0, 4) != "url:") {
					var extension = filename.substr(-4);
					var purename = filename.slice(0, -22);
					var name = purename+extension;
				} else {
					var extension = "";
					var purename = name = "Video "+filename.substr(4);
				}

				$("#blog-image-picker .photos-row").append("\
                   	<div data-id='"+image.id+"' data-position='"+position+"'>\
                      	<span class='action'>\
                       		<i class='fa fa-times'\
                       			style='color: red'\
                       			title='{{ trans("bannerphoto::bannerphotos.delete") }}'\
                       			onclick='imagePickerDelete(this)' aria-hidden='true'></i>\
                       	</span>\
                       	<img src='"+image.img+"' data-delete-filename='"+filename+"'>\
                       	<span class='title'>"+name+"</span>\
                   	</div>"
                );
			}

			$(".photos-row").sortable({
    			revert: true,
				placeholder: "ui-state-highlight",
				stop: updatePositions
    		});
		}

		function updatePositions() {
			var originalPicker = imagePickerFiles;
			imagePickerFiles = {};
			var positions = {};
			$(".photos-row div").each(function() {
				positions[$(this).data("id")] = $(this).index()+1;
				imagePickerFiles[$(this).index()+1] = originalPicker[$(this).data("position")];
			});

			$("#blog-image-picker .overlay").removeClass("hide");

			$.ajax("{{ url("/api/bannerphoto/bannerphotos") }}", {
				type: "post",
				data: {
					positions: positions,
					_token: "{{ csrf_token() }}"
				}
			}).done(function(d) {
				if (d.ok)
					toastr["success"]("{{ trans("bannerphoto::bannerphotos.messages.done.position") }}");
				else
					toastr["error"](d.message);
			}).fail(function() {
				toastr["error"]("{{ trans("bannerphoto::bannerphotos.messages.server_error") }}");
			}).always(function(d) {
				$("#blog-image-picker .overlay").addClass("hide");
				updateImages();
			});
		}

		// Delete image from server
		function imagePickerDelete(that) {
			var imageId = $(that).parent().parent().data("id");
			$("#blog-image-picker .overlay").removeClass("hide");
           	$.ajax("{{ url("/api/bannerphoto/bannerphotos") }}", {
           		data: {
           			image: imageId,
           			_token: "{{ csrf_token() }}"
          		},
           		type: "delete"
           	}).done(function(d) {
				if (d.ok) {
					toastr["success"]("{{ trans("bannerphoto::bannerphotos.messages.done.delete") }}")
               		delete imagePickerFiles[$(that).parent().parent().data("position")]; // remove image from array
					mainImageId = d.main_image;
					$(that).parent().parent().remove(); // remove element
				} else
					toastr["error"]("{{ trans("bannerphoto::bannerphotos.messages.server_error") }}")
			}).fail(function(d) {
				toastr["error"]("{{ trans("bannerphoto::bannerphotos.messages.server_error") }}")
			}).always(function() {
				$("#blog-image-picker .overlay").addClass("hide");
				updateImages();
				updatePositions();
			});
       	}


		$(function() {
			updateImages();


			// FILE SELECTING
			$("#fileinput").on("change", function(e) {
				$("#blog-image-picker .overlay").removeClass("hide");
				var file = e.target.files[0];
				var reader = new FileReader();
				reader.onload = (function(theFile) {
					return function(e) {
						$("#fileinput").val("")
						if (theFile.type.substr(0, 5) != "image") {
							$("#blog-image-picker .overlay").addClass("hide");
							return toastr["error"]("{{ trans("bannerphoto::bannerphotos.messages.not_an_image") }}");
						}

						var base64result = e.target.result
						var name = theFile.name;
           				$.ajax("{{ url("/api/bannerphoto/bannerphotos") }}", {
           	  				data: {
           	    				image: {
           	    					name: name,
									hash: base64result
								},
								_token: "{{ csrf_token() }}"
           	  				},
           	  				type: "put"
           				}).done(function(d) {
							if (d.ok)
								window.location = "";
							else
								toastr["error"]("{{ trans("bannerphoto::bannerphotos.messages.server_error") }}")
						}).fail(function(d) {
							toastr["error"]("{{ trans("bannerphoto::bannerphotos.messages.server_error") }}")
						}).always(function() {
							$("#blog-image-picker .overlay").addClass("hide");
						});
					};
				})(file);
				reader.readAsDataURL(file);
			});
		});
	</script>