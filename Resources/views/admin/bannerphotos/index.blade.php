@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('bannerphoto::bannerphotos.title.bannerphotos') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('bannerphoto::bannerphotos.title.bannerphotos') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        @include("bannerphoto::admin.bannerphotos.content")
    </div>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop

@push('css-stack')
    @include("bannerphoto::admin.bannerphotos.style")
@endpush