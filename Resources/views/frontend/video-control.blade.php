    <script>
      // Load API script
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";

      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      

      // YouTube API loaded - create video player for every video
      function onYouTubeIframeAPIReady() {
          $(".video-player[data-video-id]").each(function() {
              var videoId = $(this).data("video-id");
              var player = new YT.Player(this, {
                  videoId: videoId,
                  events: {
                      'onStateChange': onPlayerStateChange
                  },
                  playerVars: {
                      controls: 0,
                      autoplay: 1,
                      disablekb: 1,
                      showinfo: 0,
                      modestbranding: 1,
                      mute: 1,
                      rel: 0
                  }
              });
          });
      }

      // Player started/stopped playing
      function onPlayerStateChange(event) {
          // When video has ended, start it again
          if (event.data == YT.PlayerState.ENDED)
            event.target.playVideo();

          // When video has started, hide thumbnail and show video
          if (event.data == YT.PlayerState.PLAYING)
      		    $(event.target.a).css("opacity", 1);
      }
    </script>
    <style>
        iframe.video-player[data-video-id] {
            opacity: 0;
            pointer-events: none;
        }
    </style>