<?php

return [
    'title' => [
        'bannerphotos' => 'Fotky v banneru',
        'images'        => 'Obrázky',
        'texts'         => 'Popisky'
    ],
    "delete" => "Odstranit obrázek",
    "messages" => [
        "server_error" => "Chyba v komunikaci se serverem, opakujte akci!",
        "not_an_image" => "Vybraný soubor není obrázek!",
        "done" => [
            "position" => "Pořadí úspěšně upraveno!",
            "delete" => "Obrázek úspěšně odstraněn!",
        ]
    ],
    "settings_field_label" => "Texty slideru (každý slide na nový řádek)",
    "settings_video_links" => "Odkazy na videa ve slideru (každý na nový řádek)", 
];
