<?php

return [
    'title' => [
        'bannerphotos' => 'Banner photo',
        'images'        => 'Images',
        'texts'         => 'Texts'
    ],
    "delete" => "Delete a photo",
    "messages" => [
        "server_error" => "An error server has occured, please repeat your latest action!",
        "not_an_image" => "The chosen file must be an image!",
        "done" => [
            "position" => "Image position successfully updated!",
            "delete" => "An image successfully deleted!",
        ]
    ],
    "settings_field_label" => "Slides texts (separated by a new line)",
    "settings_video_links" => "Links to videos in slider (separated by a new line)",
];
