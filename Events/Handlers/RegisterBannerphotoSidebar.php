<?php

namespace Modules\Bannerphoto\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterBannerphotoSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('bannerphoto::bannerphotos.title.bannerphotos'), function (Item $item) {
                $item->icon('fa fa-picture-o');
                $item->weight(0);
                $item->authorize(
                    $this->auth->hasAccess('bannerphoto.bannerphotos.index')
                );
                $item->item(trans('bannerphoto::bannerphotos.title.images'), function (Item $item) {
                    $item->icon('fa fa-picture-o');
                    $item->weight(0);
                    $item->route('admin.bannerphoto.bannerphoto.index');
                    $item->authorize(
                        $this->auth->hasAccess('bannerphoto.bannerphotos.index')
                    );
                });
                $item->item(trans('bannerphoto::bannerphotos.title.texts'), function (Item $item) {
                    $item->icon('fa fa-file-text-o');
                    $item->weight(0);
                    $item->route('dashboard.module.settings', ['module' => 'Bannerphoto']);
                    $item->authorize(
                        $this->auth->hasAccess('bannerphoto.bannerphotos.index')
                    );
                });
            });
        });

        return $menu;
    }
}
