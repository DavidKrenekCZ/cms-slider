<?php

namespace Modules\Bannerphoto\Repositories\Eloquent;

use Modules\Bannerphoto\Repositories\BannerPhotoRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentBannerPhotoRepository extends EloquentBaseRepository implements BannerPhotoRepository
{
}
