<?php

namespace Modules\Bannerphoto\Repositories\Cache;

use Modules\Bannerphoto\Repositories\BannerPhotoRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheBannerPhotoDecorator extends BaseCacheDecorator implements BannerPhotoRepository
{
    public function __construct(BannerPhotoRepository $bannerphoto)
    {
        parent::__construct();
        $this->entityName = 'bannerphoto.bannerphotos';
        $this->repository = $bannerphoto;
    }
}
