# Banner Photo CMS module

## Installation


### Composer requirement
``` json
composer require davidkrenekcz/bannerphoto
```

### Package migrations
``` json
php artisan module:migrate Bannerphoto
```

## Usage
### Uploading photos
Uplaoding banner photos can be done through administration. After a photo is uploaded, you can change order of the photos by dragging them to their desired position.

**Note**: if your application uses `silber/page-cache` to cache pages, the cache is automatically cleared after upload, delete and position change.

### Uploading videos
Bannerphoto is capable of playing YouTube videos. Just go to administration > Settings > Bannerphoto where you'll find a textarea to write video links. Write each video link on a new line. Once you save this setting, new videos are loaded to the end of the slider and you can delete them/change their position through admin. It is **strongly recommended** to use only HQ videos, because LQ videos won't have their thumbnail loaded.

### Setting uploaded images size
You can set maximal size of uploaded images - settings are taken from `./config/asgard/bannerphoto/core.php` (see `./Modules/Bannerphoto/Config/core.example` for format)

### Setting banner texts
Banner texts are also supported - head to administration > Settings > Bannerphoto, where you'll find a textarea. You can write texts for each slide there, separated by a new line. The system does not have a validation so you need to make sure you have same number of lines as photos.

## Getting banner data
### Photos, texts and videos
To get object of photos with their corresponding labeling text, call `\Modules\Bannerphoto\Entities\BannerPhoto::bannerWithTexts()`
Example:
``` php
{{ print_r(\Modules\Bannerphoto\Entities\BannerPhoto::bannerWithTexts()) }}

// stdClass Object
// (
//     [0] => stdClass Object
//        (
//            [photo] => https:/example.com/modules/bannerphoto/uploads/2017-09-25-image1-1509223303-234105.jpg
//            [text] => Lorem ipsum,
//			  [video] => false	// this is not a video
//        )
//
//     [1] => stdClass Object
//        (
//            [photo] => https:/example.com/modules/bannerphoto/uploads/2016-06-28-image2-1509226117-822424.png
//            [text] => Dolor sit amet
//            [video] => "M7lc1UVf-VE" // ID of YouTube video - for usage see bellow
//        )
//
// )
```

### Photos
To get only array of uploaded photos'/videos' thumbnails URLs call `\Modules\Bannerphoto\Entities\BannerPhoto::photos()`
Example:
``` php
{{ print_r(\Modules\Bannerphoto\Entities\BannerPhoto::photos()) }}

// Array
// (
//    [0] => https:/example.com/modules/bannerphoto/uploads/2017-09-25-image1-1509223303-234105.jpg
//    [1] => https:/example.com/modules/bannerphoto/uploads/2016-06-28-image2-1509226117-822424.png
// )
```

### Texts
To get only array of texts from settings, call `\Modules\Bannerphoto\Entities\BannerPhoto::texts()`
Example:
``` php
{{ print_r(\Modules\Bannerphoto\Entities\BannerPhoto::texts()) }}

// Array
// (
//    [0] => Lorem ipsum
//    [1] => Dolor sit amet
// )
```

### Videos
You can use BannerPhoto's views *bannerphoto::frontend.video* and *bannerphoto::frontend.video-control* to get YouTube player functionality to your slider.
Example:
``` php
	@foreach($slider as $slide)
  	<div>
  		<img src="{{ $slide->photo }}">
  		@if ($slide->video)
  			@include("bannerphoto::frontend.video", [ "slide" => $slide ])
  		@endif
  	</div>
  	@endforeach
  	@include("bannerphoto::frontend.video-control")
```
*video* view creates a simple template for every video and *video-control* (loaded only once!) takes care of player functionality. While video is loading, thumbnail is shown and video has opacity 0. After the video is loaded, the video's opacity is set to 1 and the video is played mute in a loop. The video can't be controlled by user.
You just need to ensure that you style both *img* thumbnail and *iframe* player to your webpage so that they overlay.

**Warning**: Videos can't be automatically played on mobile devices, therefore only thumbnail is shown.

### Global `$bannerPhotos` variable
**Removed**: global variable `$bannerPhotos` has been removed in 1.1.0 due to speed efficiency, please use methods mentioned above

Banners are available through the whole application in `$bannerPhotos` variable. This variable is an array containing links to banner images ordered by position, so it could look something like this:

``` php
{{ print_r($bannerPhotos) }}

// Array
// (
//    [0] => https:/example.com/modules/bannerphoto/uploads/2017-09-25-image1-1509223303-234105.jpg
//    [1] => https:/example.com/modules/bannerphoto/uploads/2016-06-28-image2-1509226117-822424.png
// )
```