<?php

return [
	'banner-texts' => [
        'description' => trans('bannerphoto::bannerphotos.settings_field_label'),
        'view' => 'textarea',
        'translatable' => true,
        'default' => ''
    ],
    'video-links' => [
        'description' => trans('bannerphoto::bannerphotos.settings_video_links'),
        'view' => 'textarea',
        'translatable' => false,
        'default' => ''
    ],
];