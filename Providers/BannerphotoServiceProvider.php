<?php

namespace Modules\Bannerphoto\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Bannerphoto\Events\Handlers\RegisterBannerphotoSidebar;
use Modules\Bannerphoto\Composers\BannerPhotoViewComposer;

class BannerphotoServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterBannerphotoSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('bannerphotos', array_dot(trans('bannerphoto::bannerphotos')));
            // append translations
        });

        //view()->composer("*", BannerPhotoViewComposer::class);
    }

    public function boot()
    {
        $this->publishConfig('bannerphoto', 'permissions');
        $this->publishConfig('bannerphoto', 'settings');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Bannerphoto\Repositories\BannerPhotoRepository',
            function () {
                $repository = new \Modules\Bannerphoto\Repositories\Eloquent\EloquentBannerPhotoRepository(new \Modules\Bannerphoto\Entities\BannerPhoto());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Bannerphoto\Repositories\Cache\CacheBannerPhotoDecorator($repository);
            }
        );
// add bindings

    }
}
