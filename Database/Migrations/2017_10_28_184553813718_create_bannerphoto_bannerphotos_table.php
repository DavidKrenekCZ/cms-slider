<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerphotoBannerPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bannerphoto__bannerphotos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
           
            $table->string("image_name");
            $table->integer("position");

            $table->timestamps();
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bannerphoto__bannerphotos');
    }
}
