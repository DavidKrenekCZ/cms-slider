<?php

namespace Modules\Bannerphoto\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BannerPhoto extends Model
{
    use SoftDeletes;

    private const PUBLIC_PATH = "modules/bannerphoto/uploads";

    protected $table = 'bannerphoto__bannerphotos';
    protected $dates = ['deleted_at'];
    protected $fillable = [
    	'image_name',
    	'position'
    ];

    protected $appends = ["thumbnail", "isVideo", "videoUrl"];

    // Folders methods
    public static function getImageDirPath() {
        return public_path(self::PUBLIC_PATH);
    }

    public static function getImageDirURL() {
        return url(self::PUBLIC_PATH);
    }

    // Video methods
    public static function videosIDs() {
        $setting = trim(setting("bannerphoto::video-links"));
        if ($setting == "")
            return [];
        $ids = [];
        $links = explode("\n", $setting);
        foreach ($links as $link)
            if (preg_match_all("((embed/|watch\?v=)([^?&]*))", trim($link), $matches))
                $ids[] = $matches[2][0];
        return $ids;
    }

    private static function getThumbnailById($id) {
        return "https://img.youtube.com/vi/".$id."/maxresdefault.jpg";
    }

    public static function allOrdered() {
        $images = self::orderBy("position")->get();
        $position = count($images)+1;

        foreach (self::videosIDs() as $id) {
            if (!$images->where("image_name", "url:".$id)->count()) {
                $photo = BannerPhoto::create([
                    "image_name" => "url:".$id,
                    "position" => $position++
                ]);
                $images->push($photo);
            }
        }

        return $images;
    }

    public static function allOrderedArray() {
        $return = [];

        foreach (self::allOrdered() as $photo)
            $return[] = $photo->thumbnail;

        return $return;
    }

    public function getThumbnailAttribute() {
        if ($this->isVideo)
            return self::getThumbnailById(substr($this->image_name, 4));
        return self::getImageDirURL()."/".$this->image_name;
    }

    public function getIsVideoAttribute() {
        return (substr($this->image_name, 0, 4) == "url:");
    }


    /* Public methods to get banner data */

    /**
     * Get array of photos' URLs
     * @return array
     */    
    public static function photos() {
        return self::allOrderedArray();
    }

    /**
     * Get array of slider texts
     * @return array
     */
    public static function texts() {
        $texts = setting("bannerphoto::banner-texts", locale());
        if (trim($texts) == "")
            return [];

        return explode("\n", $texts);
    }

    /**
     * Get object of photos' URLs with slider texts
     * @return object
     */
    public static function bannerWithTexts() {
        $return = [];
        $texts = explode("\n", setting("bannerphoto::banner-texts", locale()));

        foreach (self::allOrdered() as $index => $photo)
            $return[] = (object)[
                "photo" => $photo->thumbnail,
                "text" => isset($texts[$index]) ? $texts[$index] : "",
                "video" => $photo->isVideo ? substr($photo->image_name, 4) : false
            ];

        return (object)$return;
    }
}
