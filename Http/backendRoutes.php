<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/bannerphoto'], function (Router $router) {
    $router->bind('bannerphoto', function ($id) {
        return app('Modules\Bannerphoto\Repositories\BannerPhotoRepository')->find($id);
    });
    $router->get('bannerphotos', [
        'as' => 'admin.bannerphoto.bannerphoto.index',
        'uses' => 'BannerPhotoController@index',
        'middleware' => 'can:bannerphoto.bannerphotos.index'
    ]);

});
