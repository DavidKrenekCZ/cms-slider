<?php

namespace Modules\Bannerphoto\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Modules\Bannerphoto\Entities\BannerPhoto;
use Modules\Bannerphoto\Http\Requests\CreateBannerPhotoRequest;
use Modules\Bannerphoto\Http\Requests\UpdateBannerPhotoRequest;
use Modules\Bannerphoto\Repositories\BannerPhotoRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Setting\Entities\Setting;
use Intervention\Image\ImageManagerStatic as Image;

class BannerPhotoController extends AdminBaseController {
	/**
	 * @var BannerPhotoRepository
	 */
	private $bannerphoto;

	public function __construct(BannerPhotoRepository $bannerphoto) {
		parent::__construct();

		$this->bannerphoto = $bannerphoto;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$bannerphotos = BannerPhoto::allOrdered();

		return view('bannerphoto::admin.bannerphotos.index', compact('bannerphotos'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$name = $this->uploadImage($request->image);

		if (!$name) {
			return response()->json(["ok" => false]);
		}
		$position = count(BannerPhoto::allOrdered()) + 1;

		$this->bannerphoto->create([
			"image_name" => $name,
			"position"   => $position
		]);


		if (config("asgard.bannerphoto.core.resize.allow")) {
			$maxWidth = (int)config("asgard.bannerphoto.core.resize.width");
			$maxHeight = (int)config("asgard.bannerphoto.core.resize.height");
			$imgPath = BannerPhoto::getImageDirPath() . "/" . $name;

			$img = Image::make($imgPath);

			if ($maxWidth > 0 && $img->width() > $maxWidth) {
				$img->widen($maxWidth)->save($imgPath);
			}

			if ($maxHeight > 0 && $img->height() > $maxHeight) {
				$img->heighten($maxHeight)->save($imgPath);
			}
		}

		self::clearHTMLCache();

		return response()->json(["ok" => true]);
	}

	// Handle image uploading
	private function uploadImage($img) {
		$name = $this->verifyFileName(substr($img["name"], 0, -4)); // add timestamp and some random number at the and of file and then save
		$extension = substr($img["name"], -4);
		$filename = date("Y-m-d") . "-" . $name . "-" . time() . "-" . mt_rand(100000, 999999) . $extension;
		$filepath = BannerPhoto::getImageDirPath();

		if (!file_exists(public_path("modules/bannerphoto"))) {
			mkdir(public_path("modules/bannerphoto"));
		}
		if (!file_exists($filepath)) {
			mkdir($filepath);
		}

		$filepath .= "/" . $filename;

		$data = explode(",", $img["hash"])[1];

		if (!file_put_contents($filepath, base64_decode($data))) {
			return false;
		}

		return $filename;
	}

	private function verifyFileName($string) {
		$StrArr = str_split($string);
		$NewStr = '';
		foreach ($StrArr as $Char) {
			$CharNo = ord($Char);
			if ($CharNo > 31 && $CharNo < 127) {
				$NewStr .= $Char;
			}
		}

		return $NewStr;
	}

	public function updatePosition(Request $request) {
		foreach ($request->positions as $id => $position) {
			$image = BannerPhoto::findOrFail($id);
			$image->position = $position;
			$image->save();
		}

		self::clearHTMLCache();

		return response()->json(["ok" => true]);
	}

	// Remove photo from collection
	public function destroy(Request $request) {
		$path = BannerPhoto::getImageDirPath();
		$photo = BannerPhoto::findOrFail($request->image);

		if (!file_exists($path . "/deleted")) {
			mkdir($path . "/deleted");
		}

		if (file_exists($path . "/" . $photo->image_name)) {
			rename($path . "/" . $photo->image_name, $path . "/deleted/" . $photo->image_name);
		}
		elseif (substr($photo->image_name, 0, 4) == "url:") {
			$setting = "";
			foreach (explode("\n", trim(setting("bannerphoto::video-links"))) as $video) {
				if (preg_match_all("((embed/|watch\?v=)([^?&]*))", trim($video), $matches)) {
					if (substr($photo->image_name, 4) != $matches[2][0]) {
						$setting .= $video . "\n";
					}
				}
			}
			Setting::where("name", "bannerphoto::video-links")->first()->update(["plainValue" => trim($setting)]);
		}

		$this->bannerphoto->destroy($photo);

		self::clearHTMLCache();
		return response()->json(["ok" => true]);
	}

	// Clear HTML cache
	public static function clearHTMLCache() {
		if (array_has(Artisan::all(), "page-cache:clear")) {
			Artisan::call("page-cache:clear");
		}
	}
}
