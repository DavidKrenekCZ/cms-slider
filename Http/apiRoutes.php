<?php

use Illuminate\Routing\Router;

$router->group(['prefix' => '/bannerphoto', 'middleware' => ['api.token']], function (Router $router) {
    $router->put('bannerphotos', [
        'as' => 'api.bannerphoto.bannerphoto.create',
        'uses' => '\Modules\Bannerphoto\Http\Controllers\Admin\BannerPhotoController@store',
        'middleware' => 'token-can:bannerphoto.bannerphotos.index',
    ]);
    $router->delete('bannerphotos', [
        'as' => 'api.bannerphoto.bannerphoto.delete',
        'uses' => '\Modules\Bannerphoto\Http\Controllers\Admin\BannerPhotoController@destroy',
        'middleware' => 'token-can:bannerphoto.bannerphotos.index',
    ]);
    $router->post('bannerphotos', [
        'as' => 'api.bannerphoto.bannerphoto.position',
        'uses' => '\Modules\Bannerphoto\Http\Controllers\Admin\BannerPhotoController@updatePosition',
        'middleware' => 'token-can:bannerphoto.bannerphotos.index',
    ]);
});
